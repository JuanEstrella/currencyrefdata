[![FINOS - Forming](https://cdn.jsdelivr.net/gh/finos/contrib-toolbox@master/images/badge-forming.svg)](https://github.com/finos/community/blob/master/governance/Software-Projects/Project-Lifecycle.md#forming-projects-optional)

# Currency Reference Data 

Short blurb about what your project does.

## Background 

## Roadmap

List the roadmap steps; alternatively link the Confluence Wiki page where the project roadmap is published.

1. Item 1
2. Item 2
3. ....

# Get Involved: Contribute to the Currency Reference Data Standard project
There are several ways to contribute to Currency Reference Data standard project:

* **Join the next meeting**: 

To do: Set meetings

Find the next meeting on the [FINOS projects calendar]({https://calendar.google.com/calendar/u/0/embed?src=finos.org_fac8mo1rfc6ehscg0d80fi8jig@group.calendar.google.com&ctz=America/New_York}) and browse [past meeting minutes in GitHub](https://github.com/finos/currencyrefdata/labels/meeting).

* **Join the mailing list**: Communications for the Currency Reference Data project are conducted through the Currency Reference Data@finos.org mailing list. Please email [currency-ref-data@finos.org](mailto:currency-ref-data@finos.org) to join the mailing list.

* **Raise an issue**: if you have any questions or suggestions, please [raise an issue](https://gitlab.com/finosfoundation/legend/financial-objects/currencyrefdata/-/issues?sort=created_date&state=opened)

## License

This project uses the **Community Specification License 1.0** ; you can read more in the [LICENSE](LICENSE) file.
